import * as fs from 'fs';

function calculateAnswers({data, getAnswer1, getAnswer2}) {
    getAnswer1(data);
    if (getAnswer2) {
        getAnswer2(data);
    }
}

export function readInputFile({day:dayNo, getAnswer1, getAnswer2, test}) {
    const filename = `./day${dayNo}${test ? '-test' : ''}-input.txt`;
    
    fs.readFile(filename, 'utf8', function (err,data) {
        if (err) {
            return err;
        }

        calculateAnswers({data, getAnswer1, getAnswer2});
    });
}