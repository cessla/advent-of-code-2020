import {readInputFile} from './helper-functions.js';

const getAnswer1 = (input) => console.log(
    input
        .split(/\r?\n\r?\n/)
        .map(str => str.replace(/\n|\r/g, ''))
        .map(str => new Set(str.split('')))
        .map(set => [...set].length)
        .reduce((acc, item) => acc + item)
    );

const getAnswer2 = (input) => console.log(
    input
        .split(/\r?\n\r?\n/)
        .map(str => str.split('\n'))
        .map(group => group.map(person => new Set(person.split(''))))
        .map(group => group.reduce((acc, set) => new Set([...acc].filter(x => set.has(x)))).size)
        .reduce((acc, item) => acc + item)
    );

readInputFile({day:6, getAnswer1, getAnswer2, test: true});
readInputFile({day:6, getAnswer1, getAnswer2})
